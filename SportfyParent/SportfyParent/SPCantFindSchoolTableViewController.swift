//
//  SPCantFindSchoolTableViewController.swift
//  SportfyParent
//
//  Created by XLR8 on 27/06/18.
//  Copyright © 2018 sics. All rights reserved.
//

import UIKit

class SPCantFindSchoolTableViewController: UITableViewController {
    @IBOutlet weak var txtFldSchoolName: UITextField!
    @IBOutlet weak var txtFldSchoolLocation: UITextField!
    @IBOutlet weak var txtFldEmailPhonenum: UITextField!
    @IBOutlet weak var btnRequestSchool: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    func setupUI(){
        btnRequestSchool.layer.cornerRadius = 20.0
        btnRequestSchool.layer.masksToBounds = true
        txtFldSchoolName.layer.cornerRadius = 20.0
        txtFldSchoolName.layer.masksToBounds = true
        txtFldEmailPhonenum.layer.cornerRadius = 20.0
        txtFldEmailPhonenum.layer.masksToBounds = true
        txtFldSchoolLocation.layer.cornerRadius = 20.0
        txtFldSchoolLocation.layer.masksToBounds = true
        txtFldSchoolName.layer.borderWidth = 0.5
        txtFldSchoolName.layer.borderColor = UIColor.black.cgColor
        txtFldSchoolLocation.layer.borderWidth = 0.5
        txtFldSchoolLocation.layer.borderColor = UIColor.black.cgColor
        txtFldEmailPhonenum.layer.borderWidth = 0.5
        txtFldEmailPhonenum.layer.borderColor = UIColor.black.cgColor
        
        let paddingView                 = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: self.txtFldSchoolName.frame.height))
        txtFldSchoolName.leftView            = paddingView
        txtFldSchoolName.leftViewMode        = UITextFieldViewMode.always
        
        let paddingView1                 = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: self.txtFldSchoolLocation.frame.height))
        txtFldSchoolLocation.leftView            = paddingView1
        txtFldSchoolLocation.leftViewMode        = UITextFieldViewMode.always
        
        let paddingView2                 = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: self.txtFldEmailPhonenum.frame.height))
        txtFldEmailPhonenum.leftView            = paddingView2
        txtFldEmailPhonenum.leftViewMode        = UITextFieldViewMode.always
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
}

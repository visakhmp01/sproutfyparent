//
//  ViewController.swift
//  Sproutfy
//
//  Created by SRISHTI INNOVATIVE on 18/06/18.
//  Copyright © 2018 SRISHTI INNOVATIVE. All rights reserved.
//

import UIKit

class SPSelectionViewController: UIViewController {

    @IBOutlet weak var btnSwitch: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    @IBAction func actionSwitch(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected == true {
            btnSwitch.setImage(#imageLiteral(resourceName: "Parent_select"), for: .normal)
        }
        else {
            btnSwitch.setImage(#imageLiteral(resourceName: "Teacher_select"), for: .normal)
        }
    }
    
}


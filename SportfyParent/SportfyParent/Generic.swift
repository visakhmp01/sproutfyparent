//
//  Generic.swift
//  SafeFunRide
//
//  Created by Vignesh's Mac on 18/05/2017.
//  Copyright © 2017 Srishti. All rights reserved.
//

import Foundation
import UIKit
//import SVProgressHUD

class GenericFunctions: NSObject {
    
    static var kbadgeCount : Int!
    
    //MARK:- Alert View Controller
    static  func showAlertView(targetVC: UIViewController, title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction((UIAlertAction(title: "OK", style: .default, handler: {(action) -> Void in
        })))
        targetVC.present(alert, animated: true, completion: nil)
}

    //MARK:- Email Validation
    static func isValidEmail(email: String) -> Bool
    {
        let stricterFilterString:NSString = "[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}"
        let emailRegex:NSString = stricterFilterString
        let emailTest:NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: email)
    }
    
      //MARK:- Store Current User Data
    static func saveLoggedUserdetails(dictDetails : NSDictionary){
        let data : NSData = NSKeyedArchiver.archivedData(withRootObject: dictDetails) as NSData
        UserDefaults.standard.set(data, forKey: "CurrentUserDetails")
        UserDefaults.standard.synchronize()
    }
    
//    static func showhud() {
//        SVProgressHUD.show()
//       // SVProgressHUD.setDefaultMaskType(.clear)
//    }
//
//    static func hidehud() {
//        SVProgressHUD.dismiss()
//    }

    
//    class func loadCount(servicename : String , parameters : NSDictionary , completion : @escaping (Bool ,AnyObject? ,NSError?) -> Void){
//        AlamofireSubclass.postMethod(servicename, with: parameters) { (success, result, error) in
//            if success == true {
//                if result != nil {
//                   OYUser.currentUser.count = result as! NSDictionary
//                    completion(true,result,nil)
//                }
//                else {
//                    print(error!)
//                }
//            }
//            else {
//                
//            }
//        }
//    }
}


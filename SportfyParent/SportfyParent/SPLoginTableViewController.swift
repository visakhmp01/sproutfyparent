//
//  SPLoginTableViewController.swift
//  Sproutfy
//
//  Created by SRISHTI INNOVATIVE on 18/06/18.
//  Copyright © 2018 SRISHTI INNOVATIVE. All rights reserved.
//

import UIKit

class SPLoginTableViewController: UITableViewController {
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtFldPassword: UITextField!
    @IBOutlet weak var txtFldTeacherID: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUi()
    }

    func setUpUi() {
        btnLogin.layer.cornerRadius = 20.0
        btnLogin.layer.masksToBounds = true
        txtFldPassword.layer.cornerRadius = 20.0
        txtFldPassword.layer.masksToBounds = true
        txtFldTeacherID.layer.cornerRadius = 20.0
        txtFldTeacherID.layer.masksToBounds = true
        txtFldPassword.layer.borderWidth = 0.5
        txtFldPassword.layer.borderColor = UIColor.black.cgColor
        txtFldTeacherID.layer.borderWidth = 0.5
        txtFldTeacherID.layer.borderColor = UIColor.black.cgColor
        
        let paddingView                 = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: self.txtFldTeacherID.frame.height))
        txtFldTeacherID.leftView            = paddingView
        txtFldTeacherID.leftViewMode        = UITextFieldViewMode.always
        
        let paddingView1                 = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: self.txtFldPassword.frame.height))
        txtFldPassword.leftView            = paddingView1
        txtFldPassword.leftViewMode        = UITextFieldViewMode.always
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}

//
//  SPSearchSchoolViewController.swift
//  Sproutfy
//
//  Created by SRISHTI INNOVATIVE on 18/06/18.
//  Copyright © 2018 SRISHTI INNOVATIVE. All rights reserved.
//

import UIKit

class SPSearchSchoolTableViewController: UITableViewController {
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUi()
        
    }
    
    func setUpUi() {
        viewSearch.layer.cornerRadius = 20.0
        viewSearch.layer.masksToBounds = true
        viewSearch.layer.borderWidth = 1.0
        viewSearch.layer.borderColor = UIColor(red: 236/255, green: 177/255, blue: 126/255, alpha: 1.0).cgColor
        btnSearch.layer.cornerRadius = 20.0
        btnSearch.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

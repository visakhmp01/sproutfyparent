//
//  SPSignInTableViewController.swift
//  Sproutfy
//
//  Created by SRISHTI INNOVATIVE on 18/06/18.
//  Copyright © 2018 SRISHTI INNOVATIVE. All rights reserved.
//

import UIKit

class SPSignInTableViewController: UITableViewController {

    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnSignUp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUi()
        print("")
        
    }
    
    func setUpUi() {
        btnSignIn.layer.cornerRadius = 20.0
        btnSignIn.layer.masksToBounds = true
        btnSignUp.layer.cornerRadius = 20.0
        btnSignUp.layer.masksToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

   
}
